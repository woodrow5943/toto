import './App.css';
import {useEffect} from 'react'

function App() {
  useEffect(()=>{
    console.log('effect', window)
    let params = (new URL(document.location)).searchParams;
    const server = params.get('server') || "https://sportboomterminal.sportdigi.com/";
    const token = params.get('token') || "-";

    var parameters = {
      /* Required parameters */
      // {String} The sport URL provided by Digitain.
          server: server,
    
      // {String} DOM element Id where application will be rendered.
      containerId: "sport_div_iframe",
    
      /* Optional parameters */
    
      // {String} User authorization token or '-' for unauthorized users.
      token: token,
    
      // {String} The default language ISO code.
      defaultLanguage: "en",
    
    
      // {Boolean} Disables hash router to prevent main URL from changing on routes 	navigation. 
      // * useful in case of integrating into existing SPA.
      hashRouterDisabled: false
    };

    window.Bootstrapper.boot(parameters, { name: "ESport" });
  })
  return (
      <div id="sport_div_iframe"></div>
  );
}

export default App;
